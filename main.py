def multiply(a, b):
    """
    >>> multiply(4, 3)
    14
    >>> multiply('a', 3)
    'aaa'
    """
    
    return a * b